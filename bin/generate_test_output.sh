#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /tmp/tests && pytest -m "not livetest" --junitxml=/tmp/tests/junit.xml --cov=minty_config --cov-report=term
