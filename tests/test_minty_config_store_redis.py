import os
import re

import pytest

from minty_config.store import RedisStore


@pytest.mark.live
class TestRedisRetrieving:
    __slots__ = ["store"]

    def setup_class(self):
        filename = os.path.dirname(__file__) + "/config/first.conf"

        with open(filename, "r", encoding="utf-8") as file:
            config = file.read()

        redis = {"saas:settings:test.example.com": config.encode("utf-8")}

        self.store = RedisStore(redis)

    def test_get_configuration(self):
        config = self.store.retrieve("test.example.com")
        assert config, "Redis did not return configuration"
        assert re.search(r"Example", config), "Config did not match 'Example'"
