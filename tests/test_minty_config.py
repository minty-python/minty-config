import os

import pytest
from redis import StrictRedis

from minty_config import Configuration, ConfigurationNotFound
from minty_config.parser import ApacheConfigParser
from minty_config.store import FileStore, RedisStore


class TestInitializationRedis:
    def test_load_minty_configuration(self):
        redis = {}
        Configuration(store=RedisStore(redis), parser=ApacheConfigParser())


class TestConfigurationRedis:
    def setup_class(self):
        filename = os.path.dirname(__file__) + "/config/first.conf"
        with open(filename, "r", encoding="utf-8") as file:
            config = file.read()

        # The Redis store only uses the "get" - which "dict" also has.
        redis = {"saas:settings:test.example.com": config.encode("utf-8")}

        self.config = Configuration(
            store=RedisStore(redis), parser=ApacheConfigParser()
        )

    def test_get_invalid_parsed_configuration(self):
        with pytest.raises(FileNotFoundError):
            self.config.get(name="does-not-exist.example.com")

    def test_get_parsed_configuration(self):
        config = self.config.get(name="test.example.com")

        assert config["name"] == "Example"


class TestConfigurationFile:
    def test_get_parsed_configuration_from_file(self):
        filestore = FileStore(os.path.dirname(__file__) + "/config")

        static_config = Configuration(
            store=filestore, parser=ApacheConfigParser()
        )

        assert static_config.get("first")["name"] == "Example"


@pytest.mark.livetest
class TestConfigurationRedisLive:
    def setup_class(self):
        filename = os.path.dirname(__file__) + "/config/first.conf"
        with open(filename, "r", encoding="utf-8") as file:
            config = file.read()

        redis = StrictRedis(host="redis", port=6379)
        redis.set("saas:test.example.com", config)

        self.config = Configuration(
            store=RedisStore(redis), parser=ApacheConfigParser()
        )

    def test_get_invalid_parsed_configuration(self):
        with pytest.raises(ConfigurationNotFound):
            self.config.get(name="does-not-exist.example.com")

    def test_get_parsed_configuration(self):
        config = self.config.get(name="test.example.com")

        assert config["name"] == "Example"
