import os

import pytest

from minty_config.parser import ApacheConfigParser


def load_first_config():
    filename = os.path.dirname(__file__) + "/config/first.conf"
    with open(filename, "r", encoding="utf-8") as file:
        return file.read()


def load_second_config():
    filename = os.path.dirname(__file__) + "/config/second.conf"
    with open(filename, "r", encoding="utf-8") as file:
        return file.read()


class TestApacheLoading:
    def test_load_config_content(self):
        parser = ApacheConfigParser()
        assert parser

    def test_load_config_file_without_config(self):
        with pytest.raises(ValueError):
            ApacheConfigParser().parse(content="")


class TestApacheParsing:
    def test_parse_config_file(self):
        parser = ApacheConfigParser()
        config = parser.parse(content=load_first_config())
        assert config["name"] == "Example"
        assert config["simple_version"] == "3"
        assert config["HashExample"]["hash_value"] == "1"
        assert config["HashExample"]["hash_value2"] == "2"
        assert config["MultiHash"]["multientity1"]["multivalue"] == "1"
        assert config["MultiHash"]["multientity2"]["multivalue"] == "1"
