import os
import re

import pytest

from minty_config.store import FileStore


class TestFilestore:
    def test_load_invalid_file(self):
        with pytest.raises(FileNotFoundError):
            filestore = FileStore(os.path.dirname(__file__) + "/config")
            filestore.retrieve("does_not_exist")

    def test_get_invalid_configuration_name(self):
        filestore = FileStore(os.path.dirname(__file__) + "/config")

        with pytest.raises(NameError):
            filestore.retrieve("../../invalid")

    def test_load_valid_file(self):
        filestore = FileStore(os.path.dirname(__file__) + "/config")
        config = filestore.retrieve("first")

        assert re.search(r"Example", config)
